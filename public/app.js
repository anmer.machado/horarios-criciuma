(function(){
    angular.module('minhaApp', [
        'ui.router'
    ]);

    angular.module('minhaApp')
        .config(MinhaAppConfig);
    
    MinhaAppConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function MinhaAppConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state({
                name: 'login',
                url: '/login',
                templateUrl: '/views/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app',
                url: '/',
                templateUrl: '/views/menu.html'
            })
            .state({
                name: 'app.horarios',
                url: 'horarios',
                template: '<ui-view></ui-view>',
                redirectTo: 'app.horarios.list'
            })
            .state({
                name: 'app.horarios.list',
                url: '/list',
                templateUrl: '/views/horarios/list.html',
                controller: 'HorariosListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.horarios.new',
                url: '/new',
                templateUrl: '/views/horarios/form.html',
                controller: 'HorariosFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.horarios.edit',
                url: '/:id',
                templateUrl: '/views/horarios/form.html',
                controller: 'HorariosFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.partidas',
                url: 'partidas',
                template: '<ui-view></ui-view>',
                redirectTo: 'app.partidas.list'
            })
            .state({
                name: 'app.partidas.list',
                url: '/list',
                templateUrl: '/views/partidas/list.html',
                controller: 'PartidasListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.partidas.new',
                url: '/new',
                templateUrl: '/views/partidas/form.html',
                controller: 'PartidasFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.partidas.edit',
                url: '/:id',
                templateUrl: '/views/partidas/form.html',
                controller: 'PartidasFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.diasSemana',
                url: 'dias',
                template: '<ui-view></ui-view>',
                redirectTo: 'app.diasSemana.list'
            })
            .state({
                name: 'app.diasSemana.list',
                url: '/list',
                templateUrl: '/views/dias-semana/list.html',
                controller: 'DiasSemanaListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.diasSemana.new',
                url: '/new',
                templateUrl: '/views/dias-semana/form.html',
                controller: 'DiasSemanaFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.diasSemana.edit',
                url: '/:id',
                templateUrl: '/views/dias-semana/form.html',
                controller: 'DiasSemanaFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.linhas',
                url: 'linhas',
                template: '<ui-view></ui-view>',
                redirectTo: 'app.linhas.list'
            })
            .state({
                name: 'app.linhas.list',
                url: '/list',
                templateUrl: '/views/linhas/list.html',
                controller: 'LinhasListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.linhas.new',
                url: '/new',
                templateUrl: '/views/linhas/form.html',
                controller: 'LinhasFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.linhas.edit',
                url: '/:id',
                templateUrl: '/views/linhas/form.html',
                controller: 'LinhasFormController',
                controllerAs: 'vm'
            })
    
    }
})();
