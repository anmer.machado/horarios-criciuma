(function () {

    angular.module('minhaApp')
        .controller('LinhasFormController', LinhasFormController);

    LinhasFormController.$inject = ['LinhasService', '$state', '$stateParams']

    function LinhasFormController(LinhasService, $state, $stateParams) {
        var vm = this;
        vm.linhas = {};
        vm.titulo = 'Inserindo linha';

        if ($stateParams.id) {
            vm.titulo = 'Editando linha';
            LinhasService.findById($stateParams.id)
                .then(function (linhas) {
                    vm.linhas = linhas;
                });
        }

        vm.salvar = function () {
            if (vm.linhas._id) {
                LinhasService.update(vm.linhas)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Linha atualizada com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.linhas')
                    });
            } else {
                LinhasService.insert(vm.linhas)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Linha inserida com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.linhas')
                    });
            }
        };
    }
})();
