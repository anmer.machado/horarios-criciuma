(function() {

    angular.module('minhaApp')
        .factory('LinhasService', LinhasService);

    LinhasService.$inject = ['$http'];

    function LinhasService($http) {

        var _URL = '/linhas';

        function findAll () {
            return $http.get(_URL)
                .then(function (response) {
                    return response.data;
                });
        }

        function findById (id) {
            return $http.get(_URL + '/' + id)
                .then(function (response) {
                    return response.data;
                });
        }

        function insert (linhas) {
            return $http.post(_URL, linhas)
                .then(function (response) {
                    return response.data;
                });
        }

        function update (linhas) {
            return $http.put(_URL + '/' + linhas._id, linhas)
                .then(function (response) {
                    return response.data;
                });
        }

        function remove (id) {
            return $http.delete(_URL + '/' + id);
        }

        return {
            findAll: findAll,
            findById: findById,
            insert :insert,
            update: update,
            remove: remove
        };
    }
})();
