(function () {

    angular.module('minhaApp')
        .controller('DiasSemanaFormController', DiasSemanaFormController);

    DiasSemanaFormController.$inject = ['DiasSemanaService', '$state', '$stateParams']

    function DiasSemanaFormController(DiasSemanaService, $state, $stateParams) {
        var vm = this;
        vm.diasSemana = {};
        vm.titulo = 'Inserindo dia da semana';

        if ($stateParams.id) {
            vm.titulo = 'Editando dia da semana';
            DiasSemanaService.findById($stateParams.id)
                .then(function (diasSemana) {
                    vm.diasSemana = diasSemana;
                });
        }

        vm.salvar = function () {
            if (vm.diasSemana._id) {
                DiasSemanaService.update(vm.diasSemana)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Dia atualizado com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.diasSemana')
                    });
            } else {
                DiasSemanaService.insert(vm.diasSemana)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Dia inserido com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.diasSemana')
                    });
            }
        };
    }
})();
