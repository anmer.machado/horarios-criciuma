(function() {

    angular.module('minhaApp')
        .factory('DiasSemanaService', DiasSemanaService);

    DiasSemanaService.$inject = ['$http'];

    function DiasSemanaService($http) {

        var _URL = '/diasSemana';

        function findAll () {
            return $http.get(_URL)
                .then(function (response) {
                    return response.data;
                });
        }

        function findById (id) {
            return $http.get(_URL + '/' + id)
                .then(function (response) {
                    return response.data;
                });
        }

        function insert (diasSemana) {
            return $http.post(_URL, diasSemana)
                .then(function (response) {
                    return response.data;
                });
        }

        function update (diasSemana) {
            return $http.put(_URL + '/' + diasSemana._id, diasSemana)
                .then(function (response) {
                    return response.data;
                });
        }

        function remove (id) {
            return $http.delete(_URL + '/' + id);
        }

        return {
            findAll: findAll,
            findById: findById,
            insert :insert,
            update: update,
            remove: remove
        };
    }
})();
