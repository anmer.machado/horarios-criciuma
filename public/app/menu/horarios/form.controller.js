(function () {

    angular.module('minhaApp')
        .controller('HorariosFormController', HorariosFormController);

    HorariosFormController.$inject = ['HorariosService', '$state', '$stateParams']

    function HorariosFormController(HorariosService, $state, $stateParams) {
        var vm = this;
        vm.horario = {};

        vm.titulo = 'Inserindo horário';

        if ($stateParams.id) {
            vm.titulo = 'Editando horário';
            HorariosService.findById($stateParams.id)
                .then(function (horario) {
                    vm.horario = horario;
                });
        }

        vm.salvar = function () {
            if (vm.horario._id) {
                HorariosService.update(vm.horario)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Horário salvo com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.horarios')
                    });
            } else {
                HorariosService.insert(vm.horario)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Horário salvo com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.horarios')
                    });
            }
        };
    }
})();
