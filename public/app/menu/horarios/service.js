(function() {

    angular.module('minhaApp')
        .factory('HorariosService', HorariosService);

    HorariosService.$inject = ['$http'];

    function HorariosService($http) {

        var _URL = '/horarios';

        function findAll () {
            return $http.get(_URL)
                .then(function (response) {
                    return response.data;
                });
        }

        function findById (id) {
            return $http.get(_URL + '/' + id)
                .then(function (response) {
                    return response.data;
                });
        }

        function insert (horario) {
            return $http.post(_URL, horario)
                .then(function (response) {
                    return response.data;
                });
        }

        function update (horario) {
            return $http.put(_URL + '/' + horario._id, horario)
                .then(function (response) {
                    return response.data;
                });
        }

        function remove (id) {
            return $http.delete(_URL + '/' + id);
        }

        return {
            findAll: findAll,
            findById: findById,
            insert :insert,
            update: update,
            remove: remove
        };
    }
})();
