(function () {

    angular.module('minhaApp')
        .controller('PartidasFormController', PartidasFormController);

    PartidasFormController.$inject = ['PartidasService', '$state', '$stateParams']

    function PartidasFormController(PartidasService, $state, $stateParams) {
        var vm = this;
        vm.partidas = {};
        vm.titulo = 'Inserindo partida';

        if ($stateParams.id) {
            vm.titulo = 'Editando partida';
            PartidasService.findById($stateParams.id)
                .then(function (partidas) {
                    vm.partidas = partidas;
                });
        }

        vm.salvar = function () {
            if (vm.partidas._id) {
                PartidasService.update(vm.partidas)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Partida atualizada com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.partidas')
                    });
            } else {
                PartidasService.insert(vm.partidas)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Partida inserida com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.partidas')
                    });
            }
        };
    }
})();
