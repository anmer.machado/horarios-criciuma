(function() {

    angular.module('minhaApp')
        .factory('PartidasService', PartidasService);

    PartidasService.$inject = ['$http'];

    function PartidasService($http) {

        var _URL = '/partidas';

        function findAll () {
            return $http.get(_URL)
                .then(function (response) {
                    return response.data;
                });
        }

        function findById (id) {
            return $http.get(_URL + '/' + id)
                .then(function (response) {
                    return response.data;
                });
        }

        function insert (partidas) {
            return $http.post(_URL, partidas)
                .then(function (response) {
                    return response.data;
                });
        }

        function update (partidas) {
            return $http.put(_URL + '/' + partidas._id, partidas)
                .then(function (response) {
                    return response.data;
                });
        }

        function remove (id) {
            return $http.delete(_URL + '/' + id);
        }

        return {
            findAll: findAll,
            findById: findById,
            insert :insert,
            update: update,
            remove: remove
        };
    }
})();
