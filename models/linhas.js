const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const linhaSchema = new Schema({
    id: {
      type: String,
      required: true
    },
    descricao: {
      type: String,
      required: true}
  });
const linha = mongoose.model('linhas', linhaSchema);

module.exports = linha;