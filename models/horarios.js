const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const horarioSchema = new mongoose.Schema({
    linha: {type: Schema.Types.ObjectId,ref: 'linhas'},
    partida: {type: Schema.Types.ObjectId,ref: 'partidas'},
    diaSemana: {type: Schema.Types.ObjectId,ref: 'diasSemana'},
    hora: {type: String, required: true},
    itinerario: String
}, {timestamps: true});
const horario = mongoose.model('horarios', horarioSchema);

module.exports = horario;