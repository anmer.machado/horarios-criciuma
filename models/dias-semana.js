const mongoose = require('mongoose');

const diaSemanaSchema = new mongoose.Schema({
    id: {type: String,required: true},
    descricao: {type: String,required: true}
});
const diaSemana = mongoose.model('diasSemana', diaSemanaSchema);

module.exports = diaSemana;