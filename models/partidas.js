const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const partidasSchema = new Schema({
    id: {
      type: String,
      required: true
    },
    descricao: {
      type: String,
      required: true}
  });
const partidas = mongoose.model('partidas', partidasSchema);

module.exports = partidas;